// ---------------SERVICES---------------
const services = [
    {
        tabName: 'web design',
        img: 'img/wordpress/wordpress1.jpg',
        text: 'Web design lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    },
    {
        tabName: 'graphic design',
        img: 'img/wordpress/wordpress2.jpg',
        text: 'Graphic design od tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    },
    {
        tabName: 'online support',
        img: 'img/wordpress/wordpress3.jpg',
        text: 'Online support incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    },
    {
        tabName: 'app design',
        img: 'img/wordpress/wordpress4.jpg',
        text: 'App design excepteur sint occaecat cupidatat non od tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt init anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    },
    {
        tabName: 'online marketing',
        img: 'img/wordpress/wordpress5.jpg',
        text: 'Online marketing occaecat cupidatat non od tempor incididunt ut labore lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    },
    {
        tabName: 'seo service',
        img: 'img/wordpress/wordpress6.jpg',
        text: 'Seo service cupidatat non od tempor incididunt ut labore od tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Deserunt mollit anim id est laborum. Lorem ipsum dolor sit amet, consectetur adip isicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
    }
];

// ---------------PORTFOLIO---------------
const portfolio = [
    {
        category: 'all',
        projects: []
    },
    {
        category: 'graphic design',
        projects: [
            {
                image: 'img/graphic design/graphic-design1.jpg',
                title: 'Graphic design 1',
                subtitle: 'Graphic design 1'
            },
            {
                image: 'img/graphic design/graphic-design2.jpg',
                title: 'Graphic design 2',
                subtitle: 'Graphic design 2'
            },
            {
                image: 'img/graphic design/graphic-design3.jpg',
                title: 'Graphic design 3',
                subtitle: 'Graphic design 3'
            },
            {
                image: 'img/graphic design/graphic-design4.jpg',
                title: 'Graphic design 4',
                subtitle: 'Graphic design 4'
            },
            {
                image: 'img/graphic design/graphic-design5.jpg',
                title: 'Graphic design 5',
                subtitle: 'Graphic design 5'
            },
            {
                image: 'img/graphic design/graphic-design6.jpg',
                title: 'Graphic design 6',
                subtitle: 'Graphic design 6'
            },
            {
                image: 'img/graphic design/graphic-design7.jpg',
                title: 'Graphic design 7',
                subtitle: 'Graphic design 7'
            },
            {
                image: 'img/graphic design/graphic-design8.jpg',
                title: 'Graphic design 8',
                subtitle: 'Graphic design 8'
            },
            {
                image: 'img/graphic design/graphic-design9.jpg',
                title: 'Graphic design 9',
                subtitle: 'Graphic design 9'
            },
            {
                image: 'img/graphic design/graphic-design10.jpg',
                title: 'Graphic design 10',
                subtitle: 'Graphic design 10'
            },
            {
                image: 'img/graphic design/graphic-design11.jpg',
                title: 'Graphic design 11',
                subtitle: 'Graphic design 11'
            },
            {
                image: 'img/graphic design/graphic-design12.jpg',
                title: 'Graphic design 12',
                subtitle: 'Graphic design 12'
            }
        ]
    },
    {
        category: 'web design',
        projects: [
            {
                image: 'img/web design/web-design1.jpg',
                title: 'Web design 1',
                subtitle: 'Web Design 1'
            },
            {
                image: 'img/web design/web-design2.jpg',
                title: 'Web design 2',
                subtitle: 'Web Design 2'
            },
            {
                image: 'img/web design/web-design3.jpg',
                title: 'Web design 3',
                subtitle: 'Web Design 3'
            },
            {
                image: 'img/web design/web-design4.jpg',
                title: 'Web design 4',
                subtitle: 'Web Design 4'
            },
            {
                image: 'img/web design/web-design5.jpg',
                title: 'Web design 5',
                subtitle: 'Web Design 5'
            },
            {
                image: 'img/web design/web-design6.jpg',
                title: 'Web design 6',
                subtitle: 'Web Design 6'
            }
        ]
    },
    {
        category: 'landing pages',
        projects: [
            {
                image: 'img/landing page/landing-page1.jpg',
                title: 'Landing page 1',
                subtitle: 'Landing page 1'
            },
            {
                image: 'img/landing page/landing-page2.jpg',
                title: 'Landing page 2',
                subtitle: 'Landing page 2'
            },
            {
                image: 'img/landing page/landing-page3.jpg',
                title: 'Landing page 3',
                subtitle: 'Landing page 3'
            },
            {
                image: 'img/landing page/landing-page4.jpg',
                title: 'Landing page 4',
                subtitle: 'Landing page 4'
            },
            {
                image: 'img/landing page/landing-page5.jpg',
                title: 'Landing page 5',
                subtitle: 'Landing page 5'
            },
            {
                image: 'img/landing page/landing-page6.jpg',
                title: 'Landing page 6',
                subtitle: 'Landing page 6'
            },
            {
                image: 'img/landing page/landing-page7.jpg',
                title: 'Landing page 7',
                subtitle: 'Landing page 7'
            }
        ]
    },
    {
        category: 'wordpress',
        projects: [
            {
                image: 'img/wordpress/wordpress1.jpg',
                title: 'Wordpress 1',
                subtitle: 'Wordpress 1'
            },
            {
                image: 'img/wordpress/wordpress2.jpg',
                title: 'Wordpress 2',
                subtitle: 'Wordpress 2'
            },
            {
                image: 'img/wordpress/wordpress3.jpg',
                title: 'Wordpress 3',
                subtitle: 'Wordpress 3'
            },
            {
                image: 'img/wordpress/wordpress4.jpg',
                title: 'Wordpress 4',
                subtitle: 'Wordpress 4'
            },
            {
                image: 'img/wordpress/wordpress5.jpg',
                title: 'Wordpress 5',
                subtitle: 'Wordpress 5'
            },
            {
                image: 'img/wordpress/wordpress6.jpg',
                title: 'Wordpress 6',
                subtitle: 'Wordpress 6'
            },
            {
                image: 'img/wordpress/wordpress7.jpg',
                title: 'Wordpress 7',
                subtitle: 'Wordpress 7'
            },
            {
                image: 'img/wordpress/wordpress8.jpg',
                title: 'Wordpress 8',
                subtitle: 'Wordpress 8'
            },
            {
                image: 'img/wordpress/wordpress9.jpg',
                title: 'Wordpress 9',
                subtitle: 'Wordpress 9'
            },
            {
                image: 'img/wordpress/wordpress10.jpg',
                title: 'Wordpress 10',
                subtitle: 'Wordpress 10'
            }
        ]
    }
];
/*---------------NEWS---------------*/
const monthArray = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'June', 'July', 'Aug', 'Sept', 'Oct', 'Nov', 'Dec'];
const news = [
    {
        title: 'Amazing Blog Post 1',
        author: 'admin',
        commentCount: '2',
        image: 'img/news/news1.png'
    },
    {
        title: 'Amazing Blog Post 2',
        author: 'admin',
        commentCount: '1',
        image: 'img/news/news2.png'
    },
    {
        title: 'Amazing Blog Post 3',
        author: 'editor',
        commentCount: '0',
        image: 'img/news/news3.png'
    },
    {
        title: 'Amazing Blog Post 4',
        author: 'editor',
        commentCount: '13',
        image: 'img/news/news4.png'
    },
    {
        title: 'Amazing Blog Post 5',
        author: 'admin',
        commentCount: '7',
        image: 'img/news/news5.png'
    },
    {
        title: 'Amazing Blog Post 6',
        author: 'editor',
        commentCount: '14',
        image: 'img/news/news6.png'
    },
    {
        title: 'Amazing Blog Post 7',
        author: 'editor',
        commentCount: '2',
        image: 'img/news/news7.png'
    },
    {
        title: 'Amazing Blog Post 8',
        author: 'admin',
        commentCount: '14',
        image: 'img/news/news8.png'
    }
];
/*---------------SLIDER---------------*/
const feedbacks = [
    {
        content: 'Integer dignissim, augue tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa. Morbi pulvinar odio eget aliquam facilisis. Iusto molestiae quidem ratione sunt ullam unde ut voluptas voluptatem.',
        name: 'Hasan Ali',
        profession: 'UI/UX designer',
        image: 'img/slider1.png'
    },
    {
        content: 'Commodi excepturi facere itaque, iusto molestiae quidem ratione sunt ullam unde ut voluptas voluptatem? Autem cumque doloremque iure laudantium voluptatum. Iusto molestiae quidem ratione sunt ullam unde ut voluptas voluptatem? Tempus ultricies luctus, quam dui laoreet sem, non dictum odio nisi quis massa.',
        name: 'Angela Gogi',
        profession: 'Front end developer',
        image: 'img/slider2.jpg'
    },
    {
        content: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusamus aperiam, commodi excepturi facere itaque, iusto molestiae quidem ratione sunt ullam unde ut voluptas voluptatem? Iusto molestiae quidem ratione sunt ullam unde ut voluptas voluptatem? Autem cumque doloremque iure laudantium voluptatum.',
        name: 'Jasper Jones',
        profession: 'Back end developer',
        image: 'img/slider3.jpg'
    },
    {
        content: 'Accusamus aperiam, commodi excepturi facere itaque, iusto  molestiae quidem ratione sunt ullam unde ut voluptas voluptatem? Autem cumque doloremque iure laudantium voluptatum.  Autem cumque doloremque iure laudantium voluptatum. Iusto molestiae quidem ratione sunt ullam unde ut voluptas voluptatem.',
        name: 'Gogi Lazy',
        profession: 'Mascot',
        image: 'img/slider4.jpg'
    }
];